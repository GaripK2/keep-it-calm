extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://level.tscn")


func _on_ContinueButton_pressed():
	get_tree().change_scene("res://level.tscn")


func _on_SaveButton_pressed():
	pass

func _on_SaveQuitMenuButton_pressed():
	get_tree().change_scene("res://Menu-Lancement.tscn")


func _on_SaveQuitDesktopButton_pressed():
	get_tree().quit(NOTIFICATION_EXIT_TREE)
