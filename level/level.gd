extends Node2D

const LIMIT_LEFT = -10000
const LIMIT_TOP = -96
const LIMIT_RIGHT = 10000
const LIMIT_BOTTOM = 690


var book_already_fallen = false
const fallBook = preload("res://book_falling.tscn")

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://Menu-Pause.tscn")

func _ready():
	for child in get_children():
		if child is Player:
			var camera = child.get_node(@"Camera2D")
			camera.limit_left = LIMIT_LEFT
			camera.limit_top = LIMIT_TOP
			camera.limit_right = LIMIT_RIGHT
			camera.limit_bottom = LIMIT_BOTTOM
