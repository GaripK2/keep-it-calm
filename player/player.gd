class_name Player
extends KinematicBody2D

# Keep this in sync with the AnimationTree's state names and numbers.
enum States {
	IDLE = 0,
	WALK = 1,
	RUN = 2,
	FLY = 3,
	FALL = 4,
}

var speed = Vector2(120.0, 360.0)
var velocity = Vector2.ZERO
var falling_slow = false
var falling_fast = false
var no_move_horizontal_time = 0.0

onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
onready var sprite = $Sprite
onready var sprite_scale = sprite.scale.x

var book_already_fallen = false
const fallBook = preload("res://book_falling.tscn")


func _ready():
	$AnimationTree.active = true


func _physics_process(delta):
	velocity.y += gravity * delta
	velocity.x = (Input.get_action_strength("move_right") - Input.get_action_strength("move_left")) * speed.x
	if Input.is_action_pressed("walk"):
		velocity.x *= 0.2
	#warning-ignore:return_value_discarded
	velocity = move_and_slide(velocity, Vector2.UP)
	# Calculate flipping and falling speed for animation purposes.
	if velocity.x > 0:
		sprite.transform.x = Vector2(sprite_scale, 0)
	elif velocity.x < 0:
		sprite.transform.x = Vector2(-sprite_scale, 0)
	# Check if on floor and do mostly animation stuff based on it.
	if abs(velocity.x) > 50:
		$AnimationTree["parameters/state/current"] = States.RUN
		$AnimationTree["parameters/run_timescale/scale"] = abs(velocity.x) / 60
	elif velocity.x:
		$AnimationTree["parameters/state/current"] = States.WALK
		$AnimationTree["parameters/walk_timescale/scale"] = abs(velocity.x) / 12
	else:
		$AnimationTree["parameters/state/current"] = States.IDLE
	if position.x <= -225 and position.x >= -250:
		position.x = -481
	if position.x <= -455 and position.x >= -480:
		position.x = -224
	if position.x <= 247 and position.x >= 222:
		position.x = 609
	if position.x <= 608 and position.x >= 583:
		position.x = 222
	if position.x <= 787 and position.x >= 762:
		position.x = 1185
	if position.x <= 1184 and position.x >= 1159:
		position.x = 761
	
	for child in get_children():
		if child is Camera2D:
			var progress_bar = child.get_child(0)
			if progress_bar.get_serenity_bar() > progress_bar.serenity_progress_bar.min_value:
				progress_bar.decrease_serenity_bar(1)
			else:
				progress_bar.set_serenity_bar(progress_bar.serenity_progress_bar.max_value)
				
	
	if !book_already_fallen:
		if position.x <= 145 and position.x >= 135:
			var book = fallBook.instance()
			get_parent().add_child(book)
			book.position = Vector2(146,478)
			book_already_fallen = true
