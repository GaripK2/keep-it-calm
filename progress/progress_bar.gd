extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var serenity_progress_bar = $serenityProgressBar


# Called when the node enters the scene tree for the first time.
func _ready():
	serenity_progress_bar.value = serenity_progress_bar.max_value
	pass # Replace with function body.

func set_serenity_bar(value):
	if value > serenity_progress_bar.max_value:
		serenity_progress_bar.value = serenity_progress_bar.max_value
		return
	if value < serenity_progress_bar.min_value:
		serenity_progress_bar.value = serenity_progress_bar.min_value
		return
	serenity_progress_bar.value = value

func get_serenity_bar():
	return serenity_progress_bar.value
	
func decrease_serenity_bar(value):
	if (serenity_progress_bar.value - value) < serenity_progress_bar.min_value:
		serenity_progress_bar.value = serenity_progress_bar.min_value
		return
	serenity_progress_bar.value = serenity_progress_bar.value - value
	
func increase_serenity_bar(value):
	if (serenity_progress_bar.value + value) > serenity_progress_bar.max_value:
		serenity_progress_bar.value = serenity_progress_bar.max_value
		return
	serenity_progress_bar.value = serenity_progress_bar.value + value
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
